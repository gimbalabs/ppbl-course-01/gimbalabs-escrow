{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# OPTIONS_GHC -fno-warn-unused-imports #-}

module Escrow.TTEscrowDraft where

import              Ledger              hiding (singleton)
import              Ledger.Typed.Scripts
import              Ledger.Value        as Value
import qualified    PlutusTx
import              PlutusTx.Prelude    hiding (Semigroup (..), unless)
import              Prelude             (Show (..), Integer)


data TTEscrowParam = TTEscrowParam
  {
    ttPubKeyHash   :: !PubKeyHash
  , someOtherThing :: !Integer -- get rid of me later if needed
  }

PlutusTx.makeLift ''TTEscrowParam


data TTEscrowDatum = TTEscrowDatum
  {
    eventReference      :: !BuiltinByteString -- reference to TT Database? if necessary
  , tutorPubKeyHash     :: !PubKeyHash
  , studentPubKeyHash   :: !PubKeyHash
  , sessionFee          :: !Integer
  , ttCompletionFee     :: !Integer
  , ttLateFee           :: !Integer
  , tutorLateFee        :: !Integer
  }

PlutusTx.unstableMakeIsData ''TTEscrowDatum

data EventAction = OnTimeCancel | LateCancel | Complete
  deriving Show

PlutusTx.makeIsDataIndexed ''EventAction [('OnTimeCancel, 0), ('LateCancel, 1), ('Complete, 2)]
PlutusTx.makeLift ''EventAction

{-# INLINEABLE mkValidator #-}
mkValidator :: TTParam -> TTEscrowDatum -> EventAction -> ScriptContext -> Bool
mkValidator tt sessionDatum action ctx =
  case action of
    OnTimeCancel    -> traceIfFalse "error" signedByTT
                    -- studentPKH gets all funds

    LateCancel      -> traceIfFalse "error" signedByTT
                    -- TT gets LateFee
                    -- Tutor gets LateFee
                    -- studentPKH gets the rest

    Complete        -> traceIfFalse "error" signedByTT
                    -- TT gets fee
                    -- Tutor gets the rest

  where
    info :: TxInfo
    info = scriptContextTxInfo ctx

    signedByTT :: Bool
    signedByTT = True

data EscrowTypes

instance ValidatorTypes EscrowTypes where
    type DatumType EscrowTypes = TTEscrowDatum
    type RedeemerType EscrowTypes = EventAction

typedValidator :: TypedValidator EscrowTypes
typedValidator =
  mkTypedValidator @EscrowTypes
    $$(PlutusTx.compile [||mkValidator||])
    $$(PlutusTx.compile [||wrap||])
  where
    wrap = wrapValidator @TTEscrowDatum @EventAction

validator :: Validator
validator = validatorScript typedValidator

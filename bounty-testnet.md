# Testing Bounty Escrow Contract

Note - these docs are in draft form. To be discussed on 13-April and 14-April at Live Coding

## Implementing + Testing Treasury

### Bounty Roles:
- *Issuer* creates and funds bounties
- *Contributor* commits to and earns bounties

### Contracts
1. Treasury: should always contain exactly one UTXO
2. Bounty: contains one UTXO for EACH BOUNTY

### Testnet Tokens:
- `play` (cef5bfce1ff3fc5b128296dd0aa87e075a8ee8833057230c192c4059) token serves as Bounty Token as specified in `BountyEscrowDraft.hs` (will be replaced with `gimbal` on Mainnet)
- `PlutusPBLCourse01` (3794c001b97da7a47823ad27b29e049985a9a97f8aa6908429180e2c) serves as Auth NFT as specified in `BountyTreasury.hs` (to be replaced on Mainnet)

### Interactions
1. (On-Chain) Issuer deposits bounty funds at Treasury Contract
2. (Off-Chain) Issuer drafts bounties and places on Front End
- Contributor claims bounties on Front End, which creates a transaction that UNLOCKS TREASURY funds and LOCKS BOUNTY UTXO:
  a. TXIN from Treasury Contract and BOUNTY TXOUT with value equal to Bounty Params specified on Front End (this is not secure, but relying on trusted use of Auth token allows us to create this MVP)
  b. Creates Datum to represent Bounty (see `BountyEscrowDraft.hs`)
  c. Deposits Bounty UTXO at Bounty Contract
- Only the ISSUER can UNLOCK BOUNTY UTXOs ("good faith" MVP):
  a. Distribute to Contributor
  b. Update (by raising Bounty Value or extending Expiration)
  c. Cancel Bounty, after some deadline is reached

---

# 1

## Compile and Build the Treasury

### To Create a Validator Hash, we must use `cardano-cli transaction ...`

## Compile and build Bounty Contract Address
- This is a parameterized validator: must define the bounty payment token

```
cardano-cli address build --payment-script-file bounty-treasury.plutus --testnet-magic 1097911063 --out-file bounty-treasury.addr

cardano-cli address build --payment-script-file bounty-play-testnet.plutus --testnet-magic 1097911063 --out-file bounty-play-testnet.addr

```

Result: `addr_test1wzd82fzfkrfjqnzpvjyzauve5ssw8wkhgepkd84jggx5n3gful79d`

---

# 2

### Lock Funds at Treasury:
- Requires Datum:
```

```

- Set Variables:
```
SENDER
SENDERKEY
TXIN=
TXIN2=
TREASURY=addr_test1wrsdlj8k39g4e3rgsza9lstjqctrlml5mghvywf9m4e288gwxshfq
```

- Build Tx

- When we run this tx, does it look like --change-address can take the other play tokens

```
cardano-cli transaction build \
--alonzo-era \
--tx-in $TXIN1 \
--tx-in $TXIN2 \
--tx-out $TREASURY+"99000000 + 400 cef5bfce1ff3fc5b128296dd0aa87e075a8ee8833057230c192c4059.706c6179" \
--tx-out-datum-embed-file treasury-withdrawal-datum.json \
--tx-out $SENDER+"2000000 + 11200 cef5bfce1ff3fc5b128296dd0aa87e075a8ee8833057230c192c4059.706c6179" \
--change-address $SENDER \
--protocol-params-file protocol.json \
--out-file tx.raw \
--testnet-magic 1097911063

cardano-cli transaction sign \
--signing-key-file $SENDERKEY \
--testnet-magic 1097911063 \
--tx-body-file tx.raw \
--out-file tx.signed

cardano-cli transaction submit \
--tx-file tx.signed \
--testnet-magic 1097911063
```

---

# 3

### Unlock from Treasury and Send to Bounty
- Requires incoming Datum: treasury-withdrawal-datum.json

- Requires Redeemer
```
data BountyEscrowDatum = BountyEscrowDatum
  {
    issuerAddress       :: !PubKeyHash
  , contributorAddress  :: !PubKeyHash
  , lovelaceAmount      :: !Integer
  , tokenAmount         :: !Integer
  , expirationTime      :: !POSIXTime
  }

{"constructor":0,"fields":[{"bytes":"22117fbd0f86a213ae4f4d824cd0d38eea29e49764ae22f5f50ba3d3"},{"bytes":"358b2dbffc561e85c8251af0b79534be44fdf049e66ef3e2f7aa9418"},{"int":12000000},{"int":500},{"int":1649778363}]}
```

- Requires outgoing Datum, to create Bounty - should match Redeemer, just paying attention to where

- In `bounty-play-testnet.plutus`, we use custom Types for Datum and Redeemer. (see `/Escrow/BountyEscrowDraft.hs`)
- Create examples for each


- Issuer: Use pkh `22117fbd0f86a213ae4f4d824cd0d38eea29e49764ae22f5f50ba3d3` from Monday test-wallet
- Contributor: need a pkh
- 1649778363 posixtime 2022-04-11 update??


```
cardano-cli transaction hash-script-data --script-data-file ....
> result
```

### Set Variables:
```
TRESTXIN=a0763bfec7144f0431783ab2955a5514b967ab059a077f249a51b6abec1450da#1
CONTRIBTXIN=392845fe6960d4714f95784f4ad3f2913c722d2ad4f7065b4b9053800fedd614#0
```

### Bug Bounty: How can you drain the treasury?

#### Set Variables
```
TRESTXIN=8220660833eca5e8dbe1d0067efcbc2fb5c7a2f41f2ea42be8ae87af343cd147#1
TREASURYPLUTUSSCRIPT="bounty-treasury.plutus"
TREASURYDATUM="treasury-withdrawal-datum.json"
TREASURYREDEEMER="datum-play-test2.json"
CONTRIBTXIN=6f213bbeb895ebdb3805052f9e0c98b812d37ddaf8604157962171c3499a10ad#1
COLLATERAL=b1805a248b3da558fe11c0e705ce1e59dcc0d49529ade644c9ae29ef0a218776#0
BOUNTYCONTRACTADDR=addr_test1wzd82fzfkrfjqnzpvjyzauve5ssw8wkhgepkd84jggx5n3gful79d
AUTHTOKENASSET="3794c001b97da7a47823ad27b29e049985a9a97f8aa6908429180e2c.506c7574757350424c436f757273653031"
PAYMENTASSET="cef5bfce1ff3fc5b128296dd0aa87e075a8ee8833057230c192c4059.706c6179"
CONTRIBUTORADDR=addr_test1vra2radc0sj882n9cuwwvzwyxxz442hxy06ftuyakzjxdlcxlstkz
```

## Notes + Next Steps
- There is not yet any logic to protect this TX from Validating without tx-out-datum on Treasury Output. Need this.
- However, when datum is removed from BOUNTY output, it looks like the contract doesn't even "see" that output as a contract output. Investigate this.

Target Hash: da60dd5a402c87e266c975c25721af9d86196f78416e781d36b5e9bad78ce1fa
da60dd5a402c87e266c975c25721af9d86196f78416e781d36b5e9bad78ce1fa

```
cardano-cli transaction build \
--alonzo-era \
--tx-in $TRESTXIN \
--tx-in-script-file bounty-treasury.plutus \
--tx-in-datum-file treasury-withdrawal-datum.json \
--tx-in-redeemer-file datum-play-test3.json \
--tx-in $CONTRIBTXIN \
--tx-in-collateral $COLLATERAL \
--tx-out $BOUNTYCONTRACTADDR+"5000000 + 1 $AUTHTOKENASSET + 40 cef5bfce1ff3fc5b128296dd0aa87e075a8ee8833057230c192c4059.706c6179" \
--tx-out-datum-embed-file datum-play-test3.json \
--tx-out $TREASURY+"90000000 + 560 cef5bfce1ff3fc5b128296dd0aa87e075a8ee8833057230c192c4059.706c6179" \
--tx-out-datum-hash "da60dd5a402c87e266c975c25721af9d86196f78416e781d36b5e9bad78ce1fa" \
--change-address $CONTRIBUTOR \
--protocol-params-file protocol.json \
--testnet-magic 1097911063 \
--out-file tx.raw


cardano-cli transaction sign \
--signing-key-file $CONTRIBUTORKEY \
--testnet-magic 1097911063 \
--tx-body-file tx.raw \
--out-file tx.signed

cardano-cli transaction submit \
--tx-file tx.signed \
$TESTNET
```

### Unlock Funds
```
SENDERTXIN=05d2db9c75e40408277e9276bf5ecdb06eb18b29f72fa302b96fa36a9fa201eb#0
CONTRACTTXIN=05d2db9c75e40408277e9276bf5ecdb06eb18b29f72fa302b96fa36a9fa201eb#1
```

## Here is a working UPDATE Transaction
```
cardano-cli transaction build \
--alonzo-era \
--testnet-magic 1097911063 \
--tx-in $SENDERTXIN \
--tx-in $CONTRACTTXIN \
--tx-in-script-file bounty-play-testnet.plutus \
--tx-in-datum-file datum-play-test1.json \
--tx-in-redeemer-file update.json \
--tx-in-collateral $COLLATERAL \
--tx-out $CONTRACT+"12000000 + 500 cef5bfce1ff3fc5b128296dd0aa87e075a8ee8833057230c192c4059.706c6179" \
--tx-out-datum-embed-file datum-play-test1.json \
--change-address $SENDER \
--required-signer /home/james/hd2/monday/payment.skey \
--protocol-params-file protocol.json \
--out-file unlock.raw

cardano-cli transaction sign \
--signing-key-file $SENDERKEY \
--testnet-magic 1097911063 \
--tx-body-file unlock.raw \
--out-file unlock.signed

cardano-cli transaction submit \
--tx-file unlock.signed \
--testnet-magic 1097911063
```
## Try a working DISTRIBUTE Tx
```
cardano-cli transaction build \
--alonzo-era \
--testnet-magic 1097911063 \
--tx-in $ISSUERTXIN \
--tx-in $BOUNTYTXIN \
--tx-in-script-file bounty-play-testnet.plutus \
--tx-in-datum-file datum-play-test3.json \
--tx-in-redeemer-file distribute.json \
--tx-in-collateral $COLLATERAL \
--tx-out $CONTRIBUTOR+"12000000 + 1 3794c001b97da7a47823ad27b29e049985a9a97f8aa6908429180e2c.506c7574757350424c436f757273653031 + 51 cef5bfce1ff3fc5b128296dd0aa87e075a8ee8833057230c192c4059.706c6179" \
--change-address $SENDER \
--required-signer /home/james/hd2/monday/payment.skey \
--protocol-params-file protocol.json \
--out-file unlock.raw

cardano-cli transaction sign \
--signing-key-file $SENDERKEY \
--testnet-magic 1097911063 \
--tx-body-file unlock.raw \
--out-file unlock.signed

cardano-cli transaction submit \
--tx-file unlock.signed \
--testnet-magic 1097911063
```
